import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;


public class Client extends JFrame{

	private JTextField userText;
	private JTextArea chatWindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private String message = "";
	private String serverIP;
	private Socket connection;

	//constructor
	public Client(String host){
		super("Client window");
		serverIP = host;
		userText = new JTextField();
		userText.setEditable(false);
		userText.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent event){
					sendData(event.getActionCommand());
					userText.setText("");
					}

				}

			);

		add(userText, BorderLayout.NORTH);
		chatWindow = new JTextArea();
		add(new JScrollPane(chatWindow), BorderLayout.CENTER);
		setSize(300,150);
		setVisible(true);
	}

	//connect to server
	public void startRunning(){
		try{
			connectToServer();
			setUpStreams();
			whileChatting();
		}catch(EOFException eofException){
			showMessage("\n Client terminated connection \n");
		}catch(IOException ioException){
			ioException.printStackTrace();
		}finally{
			closeAll();
		}
	}

	//connect to server
	private void connectToServer() throws IOException{
		showMessage("Attempting connection....\n");
		connection = new Socket(InetAddress.getByName(serverIP),6789);
		showMessage("Connected to: "+ connection.getInetAddress().getHostName());
	}

	//setting up the streams
	private void setUpStreams() throws IOException{
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("\n Streams are set up \n");
	}

	//whilechatting with server

	private void whileChatting() throws IOException{
		ableToType(true);
		do{
			try{
				message = (String) input.readObject();
				showMessage("\n " + message);
			}catch(ClassNotFoundException classNotFoundException){
				showMessage("\n I dont know what was sent\n");
			}
		}while(!message.equals("SERVER: END"));
	}

		//close streams and socket after chatting
	private void closeAll(){
		showMessage("\n Closing connections....... \n");
		ableToType(false);
		try{
			output.close();
			input.close();
			connection.close();
		}catch(IOException ioException){
			ioException.printStackTrace();

		}
	}

	//sends message to client
	private void sendData(String message){
		try{
			output.writeObject("CLIENT: "+ message);
			output.flush();
			showMessage("\nCLIENT: "+ message);
		}catch(IOException ioException){
			chatWindow.append("\n Unable to send message \n");
		}
	}

	//updates chatWindow
	private void showMessage(final String text){
		SwingUtilities.invokeLater(
			new Runnable(){
				public void run(){
					chatWindow.append(text);
				}
			}
		);
	}

	// let the user type stuffs
	private void ableToType(final boolean tof){
			SwingUtilities.invokeLater(
				new Runnable(){
					public void run(){
						userText.setEditable(tof);
					}
				}
		);
	}		

}