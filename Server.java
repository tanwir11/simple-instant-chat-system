
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class Server extends JFrame{

	private JTextField userText;
	private JTextArea chatWindow;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket server;
	private Socket connection;

	//constructor
	public Server(){
		super("SERVER WINDOW");
		userText = new JTextField();
		userText.setEditable(false);
		userText.addActionListener(
			new ActionListener(){
				public void actionPerformed(ActionEvent event){
					sendMessage(event.getActionCommand());
					userText.setText("");
				}
			}

			);
		add(userText, BorderLayout.NORTH);
		chatWindow = new JTextArea();
		add (new JScrollPane(chatWindow));
		setSize(300,150);
		setVisible(true);
	}

	//set up and run the server
	public void startRunning(){
		try{
			server = new ServerSocket(6789,10);
			while(true){
				try{
					waitForConnection();
					setUpStreams();
					whileChatting();
				}catch(EOFException eofException){
					showMessage("\n Server terminated connection \n"); 
				}finally{
					closeAll();
				}
			}

		}catch(IOException ioException){
			ioException.printStackTrace();
		}
	}

	//prompting for connection
	private void waitForConnection() throws IOException{
		showMessage("Waiting for connection....\n");
		connection = server.accept();
		showMessage("Now connected to" + connection.getInetAddress().getHostName());
	}


	//building the pipes
	private void setUpStreams() throws IOException{
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("\n streams are now setup \n");
	}

	//during the chat conversation
	private void whileChatting() throws IOException{
		String message = " You are connected! ";
		sendMessage(message);
		ableToType(true);
		do{
			try{
				message = (String) input.readObject();
				showMessage("\n" + message);
			}catch(ClassNotFoundException classNotFoundException){
				showMessage(" wtf was sent! \n");
			}
		}while(!message.equals("CLIENT - END"));
	}

	//close streams and socket after chatting
	private void closeAll(){
		showMessage("\n Closing connections....... \n");
		ableToType(false);
		try{
			output.close();
			input.close();
			connection.close();
		}catch(IOException ioException){
			ioException.printStackTrace();

		}
	}

	//sends message to client
	private void sendMessage(String message){
		try{
			output.writeObject("SERVER: "+ message);
			output.flush();
			showMessage("\nSERVER: "+ message);
		}catch(IOException ioException){
			chatWindow.append("\n Unable to send message \n");
		}
	}

	//updates chatWindow
	private void showMessage(final String text){
		SwingUtilities.invokeLater(
			new Runnable(){
				public void run(){
					chatWindow.append(text);
				}
			}
		);
	}

	// let the user type stuffs
	private void ableToType(final boolean tof){
			SwingUtilities.invokeLater(
				new Runnable(){
					public void run(){
						userText.setEditable(tof);
					}
				}
		);
	}
}
